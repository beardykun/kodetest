package com.bignerdranch.android.kodekinopoiskpankratov;


import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

public class CityChoiceActivity extends AppCompatActivity {
    private ArrayAdapter adapter;
    private ListView mListView;
    private HashMap<String, String>cityMap;
    private ArrayList<String> town;
    private ArrayList<String>saveList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choice_city);

            cityMap = new HashMap<>();
            town = new ArrayList<>();

        mListView = (ListView) findViewById(R.id.list);
        new GetTown().execute();
    }
    //Посылаем запрос и получаем информацию название и id города
    private class GetTown extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... voids) {
            APIClass api = new APIClass();
            String url = "http://api.kinopoisk.cf/getCityList?countryID=2";
            try {
                String jsonString = api.makeServiceCall(url);

                if(jsonString != null){
                    JSONObject jsonObject = new JSONObject(jsonString);
                    JSONArray array = jsonObject.getJSONArray("cityData");

                    for(int i = 0; i < array.length(); i++){
                        JSONObject c = array.getJSONObject(i);
                        String idF = c.getString("cityID");
                        String name = c.getString("cityName");

                        cityMap.put(name, idF);

                        town.add(name);
                    }
                }
            }catch (final JSONException e){e.printStackTrace();}
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            //Выводим информацию в листе
            adapter = new ArrayAdapter<>(CityChoiceActivity.this, android.R.layout.simple_list_item_1, town);
            mListView.setAdapter(adapter);
            mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                    Intent intent = new Intent(CityChoiceActivity.this, FilmInUseActivity.class);
                    String townId = "";

                    Iterator iterator = cityMap.keySet().iterator();
                    while(iterator.hasNext()) {
                        String key=(String)iterator.next();
                        townId=(String)cityMap.get(town.get(position));
                    }
                    Log.i("gotId", townId);
                    intent.putExtra("cityId", townId);
                    startActivity(intent);
                }
            });
        }
    }
    //Работа с меню
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.serch_menu, menu);
        MenuItem searchItem = menu.findItem(R.id.menu_item_search);
        final SearchView searchView = (SearchView) searchItem.getActionView();
        searchView.setQueryHint("Название города");
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                updateItems(s);
                return true;
            }
            @Override
            public boolean onQueryTextChange(String s) {
                updateItems(s);
                if(s.equals("")){
                    if(saveList != null) {
                        town.clear();
                        town.addAll(saveList);
                        adapter.notifyDataSetChanged();
                    }
                }
                return true;
            }
        });
    return true;
    }
    //Поиск города
    private void updateItems(String city){
        if (null == saveList) {
            saveList = new ArrayList<>(town);
        }
        Iterator<String> iter = town.iterator();
        while (iter.hasNext()) {
            if(!iter.next().toLowerCase().contains(city.toLowerCase()))
                iter.remove();
        }
        adapter.notifyDataSetChanged();
    }
    //Назначаю титул на меню бара и скрываю ненужные кнопки
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        ActionBar actionBar = getSupportActionBar();
        try {
            actionBar.setTitle(R.string.choose_city);
        }catch (NullPointerException ex){
            ex.printStackTrace();
            Toast.makeText(this, R.string.choose_city, Toast.LENGTH_SHORT).show();
        }
        MenuItem item2 = menu.findItem(R.id.sort_by_rating);
        item2.setVisible(false);
        return true;
    }
}