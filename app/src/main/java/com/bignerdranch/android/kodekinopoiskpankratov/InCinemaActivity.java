package com.bignerdranch.android.kodekinopoiskpankratov;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

public class InCinemaActivity extends AppCompatActivity {

    private String url = "http://api.kinopoisk.cf/getSeance?filmID=";
    private String id;
    private String cityId;
    private ListView mListView;
    private ArrayList<HashMap<String, String>>seanceInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_in_cinema);
        //id города по умолчанию
        cityId = "&cityID=490";
        Bundle bundle = getIntent().getExtras();
        //Если город выбран, получаем его id
        if(null != bundle && bundle.containsKey("cityId")){
            cityId = "&cityID="+ bundle.getString("cityId");
        }
        //получаем id фильма
        if (null != bundle && bundle.containsKey("id")) {
            id = bundle.getString("id");
        }

        url = url+id+cityId+getDate();
        seanceInfo = new ArrayList<>();

        Log.i("URL", url);

        mListView = (ListView)findViewById(R.id.list_in_cinema);

        new GetSeanceInfo().execute();
    }
    //Посылаем запрос и получаем информацию о кинотеатрах и сеансах
    private class GetSeanceInfo extends AsyncTask<Void, Void, Void>{
        @Override
        protected Void doInBackground(Void... voids) {
            APIClass api = new APIClass();

            try{
                String jsonString = api.makeServiceCall(url);

                if(jsonString != null){
                    JSONObject jsonObject = new JSONObject(jsonString);
                    JSONArray array = new JSONArray();
                    if (jsonObject.has("items")) {
                        array = jsonObject.getJSONArray("items");
                    }else {
                        Intent intent = new Intent(InCinemaActivity.this, MovieDetailActivity.class);
                        intent.putExtra("id", id);
                        intent.putExtra("noData", "Информация по данному городу недоступна");
                        intent.putExtra("cityId", cityId);
                        startActivity(intent);
                    }

                    for(int i = 0; i < array.length(); i++){
                        JSONObject jsonCinema = array.getJSONObject(i);

                        HashMap<String, String>info = new HashMap<>();

                        String cinemaName ="";
                        if (jsonCinema.has("cinemaName")) {
                            cinemaName = "Название кинотеатра: "+ jsonCinema.getString("cinemaName");
                        }
                        String address = "";
                        if (jsonCinema.has("address")) {
                            address = "Адресс: "+ jsonCinema.getString("address");
                        }
                        String seance = "";
                        if(jsonCinema.has("seance")) {
                            seance = "Сеансы:\n" + jsonCinema.getString("seance");
                            String result = seance.replaceAll("\\[|]|,","  ");
                            seance = result;
                        }
                        String seance3D = "";
                        if (jsonCinema.has("seance3D")) {
                            seance3D = "Сеансы 3D:\n" + jsonCinema.getString("seance3D");
                            String result3D = seance3D.replaceAll("\\[|]|,","  ");
                            seance3D = result3D;
                        }

                        info.put("cinemaName", cinemaName);
                        info.put("address", address);
                        info.put("seance", seance);
                        info.put("seance3D", seance3D);

                        seanceInfo.add(info);
                    }
                }
            }catch (JSONException ex){ex.printStackTrace();}
            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            //Выводим полученную информацию на экран
            ListAdapter adapter = new SimpleAdapter(InCinemaActivity.this, seanceInfo, R.layout.seanse_layout, new String[]{"cinemaName", "address", "seance", "seance3D"},
                    new int[]{R.id.one, R.id.two, R.id.three, R.id.fore});

            mListView.setAdapter(adapter);
        }
    }
    //Получаем дату
    private String getDate(){
        Date date = new Date();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy");
        return "&date=" + simpleDateFormat.format(date.getTime());
    }
    //Работа с меню
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.in_cinema_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        ActionBar actionBar = getSupportActionBar();
        try {
            actionBar.setTitle(R.string.where_to_watch);
        }catch (NullPointerException ex){
            Toast.makeText(InCinemaActivity.this,R.string.where_to_watch, Toast.LENGTH_SHORT).show();
        }
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.back_to_the_list:
                Intent intent = new Intent(InCinemaActivity.this, FilmInUseActivity.class);
                intent.putExtra("cityId", cityId);
                startActivity(intent);
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
