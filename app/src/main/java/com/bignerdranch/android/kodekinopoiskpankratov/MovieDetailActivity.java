package com.bignerdranch.android.kodekinopoiskpankratov;



import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

public class MovieDetailActivity extends AppCompatActivity {

    private String url = "http://api.kinopoisk.cf/getFilm?filmID=";
    private String cityId;
    private ArrayList<String>data;
    private String titleRu;
    private String titleEn;
    private String description;
    private String id;
    private Bitmap mBitmap;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_detail);

        Bundle bundle = getIntent().getExtras();
        //Если город выбран, получаем его id
        if(null != bundle && bundle.containsKey("cityId")){
            cityId = bundle.getString("cityId");
        }
        //получаем id фильма
        if(null != bundle && bundle.containsKey("id")) {
            id = bundle.getString("id");
        }
        String noData = "";
        if(null != bundle && bundle.containsKey("noData")) {
            noData = bundle.getString("noData");
            if (noData.equals("Информация по данному городу недоступна")) {
                Toast.makeText(this, noData, Toast.LENGTH_SHORT).show();
            }
        }
        url = url + id;

        data = new ArrayList<>();
        new GetMovieDetail().execute();
    }
    ////Посылаем запрос и получаем информацию о фильме
    private class GetMovieDetail extends AsyncTask<Void, Void, Void>{
        @Override
        protected Void doInBackground(Void... voids) {
            APIClass api = new APIClass();
            try{
                String jsonString = api.makeServiceCall(url);

                if(jsonString != null){
                    JSONObject jsonObject = new JSONObject(jsonString);


                    mBitmap = getBitmapFromURL(id);
                    String nameRU = jsonObject.getString("nameRU");
                    String nameEn = "";
                    if(jsonObject.has("nameEN")) {
                        nameEn = jsonObject.getString("nameEN");
                    }
                    String year ="";
                    if(jsonObject.has("year")) {
                        year = "Год:             " + jsonObject.getString("year") + "\n";
                    }
                    String filmLength ="Время:        "+ jsonObject.getString("filmLength")+"\n";
                    String country = "";
                    if(jsonObject.has(country)) {
                        country = "Страна:       " + jsonObject.getString("country") + "\n";
                    }
                    String slogan = "";
                    if(jsonObject.has("slogan")) {
                        slogan = "Слоган:\n" + jsonObject.getString("slogan") + "\n";
                    }
                    String genre = "";
                    if(jsonObject.has("genre")) {
                        genre = "Жанр:\n" + jsonObject.getString("genre");
                    }
                    String descriptionC = "";
                    if(jsonObject.has("description")) {
                        descriptionC = "Описание:\n\n" + jsonObject.getString("description");
                    }

                    titleRu = nameRU;
                    titleEn = nameEn;

                    description = descriptionC;
                    data.add(year);
                    data.add(filmLength);
                    data.add(country);
                    data.add(slogan);
                    data.add(genre);
                }
            }catch (JSONException e){e.printStackTrace();}
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            setStuff();
        }
    }
    //Получаем постер фильма в нужном формате
    public static Bitmap getBitmapFromURL(String id) {
        try {
            URL url = new URL("https://st.kp.yandex.net/images/film_iphone/iphone360_"+id+".jpg");
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            return BitmapFactory.decodeStream(input);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
    //Выводим информацию
    private void setStuff(){
        TextView bodyTextView = (TextView) findViewById(R.id.description_name);
        TextView titleNameRu = (TextView) findViewById(R.id.title_name_ru);
        TextView titleNameEn = (TextView) findViewById(R.id.title_name_en);
        ImageView mImageView = (ImageView) findViewById(R.id.description_poster);
        String allData = "";
        for (int i = 0; i < data.size(); i++)
            allData += data.get(i)+"\n";
        TextView descText = (TextView) findViewById(R.id.text_description);
        bodyTextView.setText(allData);
        titleNameRu.setText(titleRu);
        titleNameEn.setText(titleEn);
        descText.setText(description);
        mImageView.setImageBitmap(mBitmap);
    }
    //Работа с меню
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.detail_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        ActionBar actionBar = getSupportActionBar();
        try {
            actionBar.setTitle(R.string.detail_info);
        }catch (NullPointerException ex){
            Toast.makeText(MovieDetailActivity.this,R.string.detail_info, Toast.LENGTH_SHORT).show();
        }

        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.seance_info){
            Intent intent = new Intent(MovieDetailActivity.this, InCinemaActivity.class);
            intent.putExtra("id", id);
            if(null!=cityId) {
                Log.i("reregotId", cityId);
                intent.putExtra("cityId", cityId);
            }
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }
}
