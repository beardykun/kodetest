package com.bignerdranch.android.kodekinopoiskpankratov;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Михан on 07.11.2016.
 */
//Адаптер для списка фильмов в прокате
public class MyAdapterMovie extends ArrayAdapter<Movie> {
    private Activity activity;
    private ArrayList<Movie>movie;
    private static LayoutInflater inflater = null;

    public MyAdapterMovie(Activity activity, int textViewResourceId, ArrayList<Movie>movie){
        super(activity, textViewResourceId, movie);
        try{
            this.activity = activity;
            this.movie = movie;

            inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }catch (Exception ex){ex.printStackTrace();}
    }

    @Override
    public int getCount() {
        return movie.size();
    }

    public Movie getItem(Movie position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }
    public static class ViewHolder{
        TextView nameRu;
        TextView nameEn;
        TextView year;
        TextView rating;
        TextView filmLength;
        TextView country;
        TextView genre;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        final ViewHolder holder;
        try{
            if(convertView == null){
        view = inflater.inflate(R.layout.my_movie_adapter_layout, null);
                holder = new ViewHolder();

                holder.nameRu = (TextView)view.findViewById(R.id.nameRu);
                holder.nameEn = (TextView)view.findViewById(R.id.nameEn);
                holder.year = (TextView)view.findViewById(R.id.year);
                holder.rating = (TextView)view.findViewById(R.id.rating);
                holder.filmLength = (TextView)view.findViewById(R.id.filmLength);
                holder.country = (TextView)view.findViewById(R.id.country);
                holder.genre = (TextView)view.findViewById(R.id.genre);

                view.setTag(holder);
            }else {
                holder = (ViewHolder)view.getTag();
            }

            holder.nameRu.setText(movie.get(position).getNameRU());
            holder.nameEn.setText(movie.get(position).getNameEN());
            holder.year.setText("Год: "+movie.get(position).getYear());
            holder.rating.setText("Рейтинг: "+movie.get(position).getRating());
            holder.filmLength.setText("Время: "+movie.get(position).getFilmLength());
            holder.country.setText("Страна: "+movie.get(position).getCountry());
            holder.genre.setText("Жанр: "+movie.get(position).getGenre());
        }catch (Exception ex){ex.printStackTrace();}
        return view;
    }
}
