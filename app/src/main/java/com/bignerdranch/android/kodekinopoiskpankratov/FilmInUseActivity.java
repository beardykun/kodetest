package com.bignerdranch.android.kodekinopoiskpankratov;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;

public class FilmInUseActivity extends AppCompatActivity {

    private String cityId;
    private ListView mListView;
    private ArrayAdapter adapter;
    private ArrayList<Movie>movieList;
    private ArrayList<Movie>saveList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_film_in_use);

        Bundle bundle = getIntent().getExtras();
        //Если город выбран, получаем его id
        if(null != bundle && bundle.containsKey("cityId")){
            cityId = bundle.getString("cityId");
        }
            movieList = new ArrayList<>();

        mListView = (ListView) findViewById(R.id.list_vprokate);
        new GetMovieInfo().execute();
    }
    //Посылаем запрос и получаем информацию о фильмах в прокате
    private class GetMovieInfo extends AsyncTask<Void, Void, Void>{
        @Override
        protected Void doInBackground(Void... voids) {
            String url = "http://api.kinopoisk.cf/getTodayFilms";
            APIClass api = new APIClass();
            try{
                String jsonString = api.makeServiceCall(url);

                if(jsonString != null){
                    JSONObject jsonObject = new JSONObject(jsonString);
                    JSONArray jsonArray = jsonObject.getJSONArray("filmsData");

                    for(int i = 0; i < jsonArray.length(); i++){
                        JSONObject jsonObjectThis = jsonArray.getJSONObject(i);
                        Movie movie = new Movie();

                        if (jsonObjectThis.has("id")) {
                            movie.setId(jsonObjectThis.getString("id"));
                        }

                        if(jsonObjectThis.has("nameRU")) {
                            movie.setNameRU(jsonObjectThis.getString("nameRU"));
                        }

                        if(jsonObjectThis.has("nameEN")) {
                            movie.setNameEN(jsonObjectThis.getString("nameEN"));
                        }

                        if(jsonObjectThis.has("year")) {
                            movie.setYear(jsonObjectThis.getString("year"));
                        }

                        if (jsonObjectThis.has("rating")) {
                            movie.setRating(jsonObjectThis.getString("rating"));
                        }

                        if (jsonObjectThis.has("filmLength")) {
                            movie.setFilmLength(jsonObjectThis.getString("filmLength"));
                        }

                        if (jsonObjectThis.has("country")) {
                            movie.setCountry(jsonObjectThis.getString("country"));
                        }

                        if(jsonObjectThis.has("genre")) {
                            movie.setGenre(jsonObjectThis.getString("genre"));
                        }
                        movieList.add(movie);
                    }
                }
            }catch (final JSONException e){e.printStackTrace();}
            return null;
        }

        @Override
        protected void onPostExecute(Void voids) {
            super.onPostExecute(voids);
            //Выводим полученную информацию
            adapter = new MyAdapterMovie(FilmInUseActivity.this, 0 ,movieList);
            mListView.setAdapter(adapter);
            mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {

                    Intent intent = new Intent(FilmInUseActivity.this, MovieDetailActivity.class);
                    intent.putExtra("id", movieList.get(position).getId());
                    if(null!=cityId) {
                        Log.i("cityId", cityId);
                        intent.putExtra("cityId", cityId);
                    }
                    startActivity(intent);
                }
            });
        }
    }
    //Работа с меню
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.serch_menu, menu);
        MenuItem searchItem = menu.findItem(R.id.menu_item_search);
        final SearchView searchView = (SearchView) searchItem.getActionView();
        searchView.setQueryHint("Сортировка по жанру");
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                sortByGenre(s.toLowerCase());
                return true;
            }
            @Override
            public boolean onQueryTextChange(String s) {
                sortByGenre(s.toLowerCase());
                if(s.equals("")){
                    if(saveList != null) {
                        movieList.clear();
                        movieList.addAll(saveList);
                        adapter.notifyDataSetChanged();
                    }
                }
                return true;
            }
        });
        return true;
    }
    //Назначаю титул на меню бара
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        ActionBar actionBar = getSupportActionBar();
        try {
            actionBar.setTitle(R.string.movie_you_can_watch);
        }catch (NullPointerException ex){
            Toast.makeText(FilmInUseActivity.this,R.string.movie_you_can_watch, Toast.LENGTH_SHORT).show();
        }
        return super.onPrepareOptionsMenu(menu);
    }
    //Обработка нажатий на кнопки меню
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.sort_down:
                Collections.sort(movieList, new MyReverseComp());
                adapter.notifyDataSetChanged();
                break;
            case R.id.sort_up:
                Collections.sort(movieList, new MyComp());
                adapter.notifyDataSetChanged();
                break;
            case R.id.city_choice:
                Intent intent = new Intent(FilmInUseActivity.this, CityChoiceActivity.class);
                startActivity(intent);
        }
        return true;
    }
    //Сортировка по рейтингу (возрастание)
    private class MyComp implements Comparator<Movie>{
        @Override
        public int compare(Movie movie, Movie t1) {

            return movie.getRating().compareTo(t1.getRating());
        }
    }
    //Сортировка по рейтингу (убывание)
    private class MyReverseComp implements Comparator<Movie>{
        @Override
        public int compare(Movie movie, Movie t1) {

            return t1.getRating().compareTo(movie.getRating());
        }
    }
    //Сортировка по жанру
    private void sortByGenre(String genre){
        if (null == saveList) {
            saveList = new ArrayList<>(movieList);
        }
        Iterator<Movie> iter = movieList.iterator();
        while (iter.hasNext()) {
            boolean isNotInside = true;
            String []g = iter.next().getGenre().split(", ");


            for (int i = 0; i < g.length; i++) {
                if (g[i].contains(genre))
                    isNotInside = false;
            }
            if(isNotInside)
            iter.remove();
        }
        adapter.notifyDataSetChanged();
    }
}
