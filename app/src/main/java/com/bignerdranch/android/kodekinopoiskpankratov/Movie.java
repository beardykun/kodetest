package com.bignerdranch.android.kodekinopoiskpankratov;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Михан on 07.11.2016.
 */
//Класс фильма
public class Movie implements Parcelable{
    private String id = "";
    private String nameRU = "";
    private String nameEN = " ";
    private String year = "";
    private String rating = "0 - Нет информации";
    private String filmLength = "";
    private String country = "";
    private String genre = "Нет информации";

    protected Movie(Parcel in) {
        id = in.readString();
        nameRU = in.readString();
        nameEN = in.readString();
        year = in.readString();
        rating = in.readString();
        filmLength = in.readString();
        country = in.readString();
        genre = in.readString();
    }

    public Movie() {

    }

    public String getId() {
        return id;
    }

    public String getNameRU() {
        return nameRU;
    }

    public String getNameEN() {
        return nameEN;
    }

    public String getYear() {
        return year;
    }

    public String getRating() {
        return rating;
    }

    public String getFilmLength() {
        return filmLength;
    }

    public String getCountry() {
        return country;
    }

    public String getGenre() {
        return genre;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setNameRU(String nameRU) {
        this.nameRU = nameRU;
    }

    public void setNameEN(String nameEN) {
        this.nameEN = nameEN;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public void setFilmLength(String filmLength) {
        this.filmLength = filmLength;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(id);
        parcel.writeString(nameRU);
        parcel.writeString(nameEN);
        parcel.writeString(year);
        parcel.writeString(rating);
        parcel.writeString(filmLength);
        parcel.writeString(country);
        parcel.writeString(genre);
    }
    public static final Creator<Movie> CREATOR = new Creator<Movie>() {
        @Override
        public Movie createFromParcel(Parcel in) {
            return new Movie(in);
        }

        @Override
        public Movie[] newArray(int size) {
            return new Movie[size];
        }
    };
}
